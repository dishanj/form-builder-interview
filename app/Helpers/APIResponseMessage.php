<?php
namespace App\Helpers;


class APIResponseMessage
{
    const ERROR_STATUS = "error";
    const SUCCESS_STATUS = "success";
    const NO_DATA = 'No data';
    const FORM_NAME_ALREADY_TAKEN = 'Form name already taken';


}
