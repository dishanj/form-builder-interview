<?php

namespace App\Http\Controllers\API;
use App\Helpers\APIResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FormBuilder;

class FormBuilderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'form_name' => 'required'
        ]);

        //convert name to code
        $formCode = strtoupper($request['form_name']);
        $formCode = str_replace(' ', '_', $formCode);
        //check formdata code is uniq or not
        $formdata = FormBuilder::where('form_name',$formCode)->first();
        if($formdata){
            return response()->json(['message' => APIResponseMessage::FORM_NAME_ALREADY_TAKEN], 421);
        }

       $data = FormBuilder::create($request->all());
       if($data){
        return response()->json($data, 200);
        } else {
            return response()->json(['message' => APIResponseMessage::ERROR_STATUS], 421);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = FormBuilder::find($id);
        return view('view',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
