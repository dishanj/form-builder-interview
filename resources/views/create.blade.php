@extends('layouts.master')

@section('content')

         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
               <div class="container-fluid">
                  <div class="row mb-2">
                     <div class="col-sm-6">
                        <h1 class="m-0">Form Builder</h1>
                     </div>
                     <!-- /.col -->
                     <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                           <li class="breadcrumb-item"><a href="#">Home</a></li>
                           <li class="breadcrumb-item active">Form Builder</li>
                        </ol>
                     </div>
                     <!-- /.col -->
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <section class="content">
               <div class="container-fluid">
                  <div class="row">
                     <!-- left column -->
                     <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                           <!-- /.card-header -->
                           <!-- form start -->
                           <div class="card-body">
                              <div class="form-group">
                                 <label for="formName">Form Name</label>
                                 <input type="name" name="name" class="form-control formName" id="formName" placeholder="form name">
                              </div>
                           </div>
                        </div>
                        <!-- /.card -->
                     </div>
                     <!--/.col (left) -->
                     <!-- right column -->
                     <div class="col-md-6">
                     </div>
                     <!--/.col (right) -->
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container-fluid -->
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-md-3">
                        <div class="sticky-top mb-3">
                           <div class="card">
                              <div class="card-header">
                                 <h4 class="card-title">Fields</h4>
                              </div>
                              <div class="card-body">
                                 <!-- the events -->
                                 <div id="external-events">
                                    <div class="external-event bg-success">
                                       <li class="form_bal_textfield">
                                          <a href="javascript:;">Text Field <i class="fa fa-plus-circle pull-right"></i></a>
                                       </li>
                                    </div>
                                    <div class="external-event bg-warning">
                                       <li class="form_bal_textarea">
                                          <a href="javascript:;">Text Area <i class="fa fa-plus-circle pull-right"></i></a>
                                       </li>
                                    </div>
                                    <div class="external-event bg-primary">
                                       <li class="form_bal_select">
                                          <a href="javascript:;">Select <i class="fa fa-plus-circle pull-right"></i></a>
                                       </li>
                                    </div>
                                    <div class="external-event bg-info">
                                       <li class="form_bal_date">
                                          <a href="javascript:;">Date <i class="fa fa-plus-circle pull-right"></i></a>
                                       </li>
                                    </div>
                                    <div class="external-event bg-danger">
                                       <li class="form_bal_radio">
                                          <a href="javascript:;">Radio Button <i class="fa fa-plus-circle pull-right"></i></a>
                                       </li>
                                    </div>
                                    <div class="external-event bg-danger">
                                        <li class="form_bal_button">
                                            <a href="javascript:;">Button <i class="fa fa-plus-circle pull-right"></i></a>
                                        </li>
                                    </div>
                                    <div class="clearfix">
                                       <div class="container">
                                          <button style="cursor: pointer;display: none" class="btn btn-info export_html mt-2 pull-right">Export HTML</button>
                                          <button style="cursor: pointer;display: none" class="btn btn-info submit_html mt-2 pull-right">Submit</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- /.card-body -->
                           </div>
                           <!-- /.card -->
                        </div>
                     </div>
                     <!-- /.col -->
                     <div class="col-md-4">
                        <div class="col-md-12">
                           <div class="tab-content">
                              <div class="bal_builder">
                                 <div class="form_builder_area"></div>
                              </div>
                           </div>
                        </div>
                        <!-- /.card -->
                     </div>
                     <div class="col-md-5">
                        <div class="col-md-12">
                           <form class="form-horizontal">
                              <div class="preview"></div>
                              <div style="display: none" class="form-group plain_html"><textarea id="form_html_data" rows="50" class="form-control"></textarea></div>
                           </form>
                        </div>
                        <!-- /.card -->
                     </div>
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->

        
@endsection