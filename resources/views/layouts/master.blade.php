<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
   -->
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <title>Form Builder</title>
      <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/form_builder.css') }}" rel="stylesheet">
   </head>
   <body class="hold-transition sidebar-mini">
      <div class="wrapper">
         <!-- Navbar -->
         <nav class="main-header navbar navbar-expand navbar-white navbar-light">
           
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
               <!-- Navbar Search -->
               <li class="nav-item">
                  <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                  <i class="fas fa-search"></i>
                  </a>
                  <a href="/logout">
    Logout
</a>
               </li>
            </ul>
         </nav>
         <!-- /.navbar -->


         <!-- Main Sidebar Container -->
         <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
            <img src="{{ url('/') }}/user.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">Form Builder</span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
               <!-- Sidebar user panel (optional) -->
               <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                  <div class="image">
                     <img src="{{ url('/') }}/user.png" class="img-circle elevation-2" alt="User Image">
                  </div>
                  <div class="info">
                     <a href="#" class="d-block">User</a>
                  </div>
               </div>
               
               <!-- Sidebar Menu -->
               <nav class="mt-2">
                  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                     <li class="nav-item">
                        <a href="/home" class="nav-link">
                           <i class="nav-icon fas fa-th"></i>
                           <p>
                              All Forms
                           </p>
                        </a>
                     </li>

                     <li class="nav-item">
                        <a href="/form_builder" class="nav-link">
                           <i class="nav-icon fas fa-th"></i>
                           <p>
                              Create Forms
                           </p>
                        </a>
                     </li>
                  </ul>
               </nav>
               <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
         </aside>

         @yield('content')
      </div>
      <!-- ./wrapper -->
      <!-- REQUIRED SCRIPTS -->
      <script src="{{ asset('js/app.js') }}"></script>
      <script src="{{ url('/') }}/js/jquery.min.js"></script>
      <script src="{{ url('/') }}/js/jquery-ui.min.js"></script>
      <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
      <script>
         $(document).on('click', '.export_html', function () {
           $('.submit_html').show();
           });
         
           $(document).on('click', '.submit_html', function () {
               var formName = $("#formName").val();
               var form_builder_data = JSON.stringify(  $('.plain_html').find('textarea').val());
         $.ajax({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
           url: './form_builder',
           type: "post",
           data: {
               'form_builder_data' : form_builder_data,
               'form_name' : formName
           },
           dataType : 'json',
           success: function (response) {
               Swal.fire({
               position: 'top-end',
               icon: 'success',
               title: 'Your form has been saved',
               showConfirmButton: false,
               timer: 1500
               });
               document.location.href = '/home';
           },
           error: function(response){
               if(response.status == 422){
                   Swal.fire({
                   icon: 'error',
                   title: 'Oops...',
                   text: response.responseJSON.errors.form_name
                   });
               }else{
                   Swal.fire({
                   icon: 'error',
                   title: 'Oops...',
                   text: response.responseJSON.message
                   });
                   
               }
               
           },
         });
           });
      </script>
   </body>
</html>